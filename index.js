// console.log("Last session of the week!");


// [Section]While loop

// A while loop takes in an expression/condition.
// Expressions are any unit of code that can be evaluated to a value.
// if the condition evaluates to be true the statements inside the code block will be executed.
// A loop will iterate a certain number of times until an expression/condition is met.

// Iteration is the tern given to the repitition of statements
/*
	Syntax:
	while(expression/condition){
		statements;
		iteration;
	}
*/

/*let count=5;
// while the value of count is not equal to zero
while(count!==0){
	// The current value of count is printer out.
	console.log("While: "+count);
	// decrement
	count--;
	console.log("Value of count after the Iteration: "+count);

	// Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0.
	// Loops occupy a significant amount of memory space in our devices.
	// Make sure that expressions/conditoins in loops have the corresponding increment/decrement operators to stop the loop
	// Forgetting to include this in loops will make our applications run an infinite loop.
	// after running the script, if a slow response from the browser is experienced or an infinite loop is seen in the console quickly close the application/tab/browser to avoid this.
}*/

// [Section] do while loop

/*
	- a do-while-loop works a lot like the while loop.but unlike while loops,do-while-loop guarantee that the code will be executed at least once.

	Syntax:

	do{
		statement;
		iteration;
	}
	while(expression/condition)
*/

/*let number=Number(prompt("Give me a number."));

do{
	console.log("Do while: " +number);
	number++;
}
while(number<=10);
*/

// [Section] For loop

/*
	-A for loop is more flexible than while and do-while.
	It consists of 3 parts:
	1. The "initialization" value that will track the progression of the loop.
	2.The "expression/condition" that will be evaluated which will determine whether the loop will run one more time. 
	3.The "finalExpression" indicates how to advance the loop.

	Syntax:
		for(initialization; expression/condition; finalExpression){
	statement/statements;
		}
*/

/*
	-we will create a loop that will start from 0 and end at 20
	-Every iteration of the loop,the value of count will be checked if it is equal or less than 20
	-if the value of count is less than or equal to 20 the statement inside of the loop will execute.
	-The value of count will be incremented by one for each iteration
*/

/*	for(let count=2;count<=20;count++){
		console.log("The current value of count is:" +count);
	}
*/
/*let myString ="michael";
	// characters in strings may be counted using the .length property
	// Strings are special compare to other data types,that it has access to functions and other pieces of information.

	console.log(myString.length);

	// Accessing characters of a string.
	// Individual characters of a string may be accesses using it's index number.

	// The first character in a string corresponds to 0, the next is 1 up to the nth/last character.

	// console.log(myString[0]);
	// console.log(myString[1]);
	// console.log(myString[2]);
	// console.log(myString[3]);

	for(let i = 0;i < myString.length;i++){
		console.log(myString[i]);
	}*/

	/*let numA=15;

	for(let i=0;i<=10; i++){

		let exponential= numA**i

		console.log(exponential);
	}*/


// Create a string named "myName" with value of "Alex"

let myName="Michael";

/*
	Create a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is vowel
*/

for(let i=0;i < myName.length;i++){
	if(myName[i].toLowerCase()==="a" ||
		myName[i].toLowerCase()==="e"||
		myName[i].toLowerCase()==="i"||
		myName[i].toLowerCase()==="o"||
		myName[i].toLowerCase()==="u"){
		console.log(3);
	}
	else{
		console.log(myName[i]);
	}

}

// [Section] Continue and break statements

	/*
		-The "continue" statements allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block.
		-the "break" statements is used to terminate the current loop once a match has been found.
	*/

 /*Creates a loop that if the count value is divided by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop
    - How this For Loop works:
        1. The loop will start at 0 for the the value of "count".
        2. It will check if "count" is less than the or equal to 20.
        3. The "if" statement will check if the remainder of the value of "count" divided by 2 is equal to 0 (e.g 0/2).
        4. If the expression/condition of the "if" statement is "true" the loop will continue to the next iteration.
        5. If the value of count is not equal to 0, the console will print the value of "count".
        6. The second if statement will check if the value of "count" is greater than 10. (e.g. 0)
        7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
        8. The value of "count" will be incremented by 1 (e.g. count = 1)
        9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement

*/




	for(let count=0;count<=20;count++){
		// if remainder is equal to 0
			if (count % 2 === 0 ){

			// Tells the code to continue to the next iteration of the loop
			// This ignores all statements located after the continue statement
				continue;
			}	
			else if(count>10){
				// Tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20
				// number values after 10 will no longer be printed
				break;
			}
			console.log("continue and Break " + count);
	}


	let name="alexandro";

	for(let i = 0;i<name.length;i++){
	
		if(name[i].toLowerCase()==="a"){
			console.log("Continue to next iteration.");
			continue;
    }
        if(name[i].toLowerCase()==="d"){
    		console.log("console log before the break");
    		break;

    }
	    console.log(name[i])
	}

